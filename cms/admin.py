# CMS/Admin.py
from django.contrib import admin
from .models import Page, ServicesOfferd, Gallery, Reviews, Analytics, VisitingCard, TagItems, OperationHours, \
     PageSocialLink, GalleryConfig, CustomLink, ContactUS, Address, PaymentMethod, PaymentTypes, Location


# Register your models here.
admin.site.register(Page)
admin.site.register(ServicesOfferd)
admin.site.register(Gallery)
admin.site.register(Reviews)
admin.site.register(Analytics)
admin.site.register(VisitingCard)
admin.site.register(TagItems)
admin.site.register(OperationHours)
admin.site.register(PageSocialLink)
admin.site.register(GalleryConfig)
admin.site.register(CustomLink)
admin.site.register(ContactUS)
admin.site.register(Address)
admin.site.register(PaymentMethod)
admin.site.register(PaymentTypes)
admin.site.register(Location)
