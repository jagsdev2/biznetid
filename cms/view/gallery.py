from rest_framework import generics
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from cms.models import Gallery,GalleryConfig, Page
from cms.permissions import ImageOwner

class GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = ['id','page', 'gallery_type','image']
        
    def validate(self, data):
        page = Page.objects.get(id=data['page'].id)
        added_images = page.page_gallery.all().count()
        gallery_type = GalleryConfig.objects.get(id=data['gallery_type'].id)
        if added_images == gallery_type.max_uploads:
            raise serializers.ValidationError("Maximum number of uploads are reached.")
        return data

class GalleryCreateView(generics.CreateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = GallerySerializer


class ImageDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated,ImageOwner]
    serializer_class = GallerySerializer
    queryset = Gallery.objects.all()


    # def create(self, validated_data):
    #     available_images = Gallery.objects.filter()
    #     gallery_type = Gallery