from rest_framework import generics
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from cms.permissions import TagOwner,IsServiceOwner
from cms.models import TagItems

class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagItems
        fields = "__all__"

class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagItems
        fields = "__all__"


class TagDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated,TagOwner]
    serializer_class = TagSerializer
    queryset = TagItems.objects.all()


class SericeDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated,IsServiceOwner]
    queryset = TagItems.objects.all()
    serializer_class = ServiceSerializer