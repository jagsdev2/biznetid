# CMS / Views / PageDetails.py

from django.http import HttpResponse, JsonResponse
from rest_framework import status
from django.db import IntegrityError
from rest_framework import generics
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from ..models import Page, Address, OperationHours, Location, PaymentMethod, PageSocialLink, CustomLink, Gallery, \
    ServicesOfferd, TagItems, ContactUS
from ..serializer import PageSerializer, AddressSerializer, SocialLinkSerializer, \
    OperationHoursSerializer, ReviewsSerializer, GallerySerializer, ServicesOfferdSerializer, CustomLinkSerializer, \
    ContactFormSerializer, PaymentMethodSerializer, LocationMapSerializer, ImageSerializer, TagItemsSerializer

from rbac.permissions import IsPageOwner
from rest_framework import serializers
from cms.permissions import IsPageOwner
from django.shortcuts import get_object_or_404


# Address Bar creating and Updating
class AddressCreate(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        print(request.data)
        serializer = AddressSerializer(data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                status_msg = serializer.data
                return Response(status_msg, status.HTTP_200_OK)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return Response(status_msg, status.HTTP_400_BAD_REQUEST)

    queryset = Address.objects.all()

    def get_object(self, id):
        try:
            return Address.objects.get(id=id)

        except Address.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        template = self.get_object(id)
        serializer = AddressSerializer(template)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        template = self.get_object(id)
        serializer = AddressSerializer(template, data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# new tags Experiment by rest
# class TagsDetailView(generics.RetrieveUpdateDestroyAPIView):
#     permission_classes = [IsAuthenticated]
#     serializer_class = TagItemsSerializer
#     queryset = TagItems.objects.all()
#
#     # def get(self, request, id):
#     #     queryset = TagItems.objects.filter(page=id)
#     #     serializer = TagItemsSerializer(queryset, many=True)
#     #     return Response(serializer.data)
#
#
# class TagView(generics.ListCreateAPIView):
#     permission_classes = [IsAuthenticated] #IsPageOwner
#     serializer_class = TagItemsSerializer
#     queryset = TagItems.objects.all()


# View tags only Filter by page
class Viewtags(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        tags = TagItems.objects.filter(page=id)
        serializer = TagItemsSerializer(tags, many=True, read_only=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


# tags create and Update
class TagDeatils(APIView):
    permission_classes = [IsAuthenticated]
    queryset = TagItems.objects.all()

    def post(self, request):
        serializer = TagItemsSerializer(data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                try:
                    serializer.save()
                    status_msg = serializer.data
                    return Response(status_msg, status.HTTP_200_OK)
                except IntegrityError:
                    status_msg = "You have reached the Tags limit"
                    return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return Response(status_msg, status.HTTP_404_NOT_FOUND)

    def get_object(self, id):
        try:
            return TagItems.objects.get(id=id)

        except TagItems.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        tags = self.get_object(id)
        serializer = TagItemsSerializer(tags)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        tags = self.get_object(id)
        serializer = TagItemsSerializer(tags, data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        page = self.get_object(id)
        page.delete()
        msg = "Tag Successfully Deleted"
        return Response(msg, status=status.HTTP_204_NO_CONTENT)


# Create and Update Social Links
class SocialLinkCreate(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = SocialLinkSerializer(data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                status_msg = serializer.data
                return Response(status_msg, status.HTTP_200_OK)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return Response(status_msg, status.HTTP_404_NOT_FOUND)

    queryset = PageSocialLink.objects.all()

    def get_object(self, id):
        try:
            return PageSocialLink.objects.get(id=id)

        except PageSocialLink.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        sociallink = self.get_object(id)
        serializer = SocialLinkSerializer(sociallink)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        serializer = self.get_object(id)
        serializer = SocialLinkSerializer(serializer, data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Create and Update Operation Hours
class OperationHoursCreate(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = OperationHoursSerializer(data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                status_msg = serializer.data
                return Response(status_msg, status.HTTP_200_OK)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return Response(status_msg, status.HTTP_404_NOT_FOUND)

    queryset = OperationHours.objects.all()

    def get_object(self, id):
        try:
            return OperationHours.objects.get(id=id)

        except OperationHours.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        template = self.get_object(id)
        serializer = OperationHoursSerializer(template)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        template = self.get_object(id)
        serializer = OperationHoursSerializer(template, data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Create Reviews
class ReviewsCreate(APIView):
    # permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = ReviewsSerializer(data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            serializer.save()
            status_msg = serializer.data
            return Response(status_msg, status.HTTP_200_OK)
        status_msg = serializer.errors
        return Response(status_msg, status.HTTP_404_NOT_FOUND)


# Create and Update Services
class ServicesCreate(APIView):
    permission_classes = [IsAuthenticated]
    queryset = ServicesOfferd.objects.all()

    def post(self, request):
        serializer = ServicesOfferdSerializer(data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                try:
                    serializer.save()
                    status_msg = serializer.data
                    return Response(status_msg, status.HTTP_200_OK)
                except IntegrityError:
                    status_msg = "You have reached the Tags limit"
                    return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return Response(status_msg, status.HTTP_404_NOT_FOUND)

    def get_object(self, id):
        try:
            return ServicesOfferd.objects.get(id=id)

        except ServicesOfferd.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        service = self.get_object(id)
        print("Service :", service)
        serializer = ServicesOfferdSerializer(service)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        service = self.get_object(id)
        serializer = ServicesOfferdSerializer(service, data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        service = self.get_object(id)
        service.delete()
        msg = "Service Successfully Deleted"
        return Response(msg, status=status.HTTP_204_NO_CONTENT)


# ------- Testing of Service with REST Views  ------------------------
class ServiceListView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = ServicesOfferdSerializer

    def get(self, request, id):
        queryset = ServicesOfferd.objects.filter(page=id)
        serializer = ServicesOfferdSerializer(queryset, many=True)
        return Response(serializer.data)


class ServiceDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated, IsPageOwner]
    serializer_class = ServicesOfferdSerializer
    queryset = ServicesOfferd.objects.all()


# ------------------------------------------------------------------------------------


# Create and Update Custom links
class CreateLinks(APIView):
    permission_classes = [IsAuthenticated]
    queryset = CustomLink.objects.all()

    def post(self, request):
        serializer = CustomLinkSerializer(data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                status_msg = serializer.data
                return Response(status_msg, status.HTTP_200_OK)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)

    def get_object(self, id):
        try:
            return CustomLink.objects.get(id=id)

        except CustomLink.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        links = self.get_object(id)
        serializer = CustomLinkSerializer(links)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        links = self.get_object(id)
        serializer = CustomLinkSerializer(links, data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Create Contact Form data
class ContactForm(APIView):

    def post(self, request):
        serializer = ContactFormSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            status_msg = serializer.data
            return Response(status_msg, status.HTTP_200_OK)
        status_msg = serializer.errors
        return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)


class ContactDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = ContactFormSerializer
    queryset = ContactUS.objects.all()


# Create and Update Payment Methods
class PaymentMethod(APIView):
    permission_classes = [IsAuthenticated]
    queryset = PaymentMethod.objects.all()

    def post(self, request):
        serializer = PaymentMethodSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            status_msg = serializer.data
            return Response(status_msg, status.HTTP_200_OK)
        status_msg = serializer.errors
        return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)

    def get_object(self, id):
        try:
            return PaymentMethod.objects.get(id=id)

        except PaymentMethod.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        paymnets = self.get_object(id)
        serializer = PaymentMethodSerializer(paymnets)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        paymnets = self.get_object(id)
        serializer = PaymentMethodSerializer(paymnets, data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Create and Update Locations
class LocationMap(APIView):
    permission_classes = [IsAuthenticated]
    queryset = Location.objects.all()

    def post(self, request):
        serializer = LocationMapSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            status_msg = serializer.data
            return Response(status_msg, status.HTTP_200_OK)
        status_msg = serializer.errors
        return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)

    def get_object(self, id):
        try:
            return Location.objects.get(id=id)

        except Location.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        map = self.get_object(id)
        serializer = LocationMapSerializer(map)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        map = self.get_object(id)
        serializer = LocationMapSerializer(map, data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Testing of Gallery details ---------------------------------------------------------

def modify_input_for_multiple_files(page, gallery_type, image):
    dict = {}
    dict['page'] = page
    dict['gallery_type'] = gallery_type
    dict['image'] = image
    return dict


class ImageView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def get(self, request):
        all_images = Gallery.objects.all()
        serializer = ImageSerializer(all_images, many=True)
        return JsonResponse(serializer.data, safe=False)


# Create and Update Gallery
class GalleryCreate(APIView):
    permission_classes = [IsAuthenticated]
    queryset = Gallery.objects.all()

    def post(self, request, *args, **kwargs):
        print(request.data)
        page = request.data['page']
        gallery_type = request.data['gallery_type']

        # converts querydict to original dict
        images = dict(request.data.lists())['image']
        flag = 1
        arr = []
        for img_name in images:
            modified_data = modify_input_for_multiple_files(page, gallery_type, img_name)
            file_serializer = ImageSerializer(data=modified_data)
            if file_serializer.is_valid():
                file_serializer.save()
                arr.append(file_serializer.data)
            else:
                flag = 0

        if flag == 1:
            return Response(arr, status=status.HTTP_201_CREATED)
        else:
            return Response(arr, status=status.HTTP_400_BAD_REQUEST)

    def get_object(self, id):
        try:
            return Gallery.objects.filter(id=id)

        except Gallery.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        object = self.get_object(id)
        serializer = GallerySerializer(object, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        gallery = self.get_object(id)
        serializer = GallerySerializer(gallery, data=request.data)
        if serializer.is_valid():
            id = request.data['page']
            page_owner = Page.objects.get(id=id).user
            if page_owner == request.user:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            else:
                status_msg = "Page Owner permission Error"
                return Response(status_msg, status.HTTP_406_NOT_ACCEPTABLE)
        status_msg = serializer.errors
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        gallery = self.get_object(id)
        gallery.delete()
        msg = "Page Successfully Deleted"
        return Response(msg, status=status.HTTP_204_NO_CONTENT)


# New codes by Sreejin -----------------------

class FileSerializer(serializers.ModelSerializer):
    # file = serializers.ListField(child=serializers.FileField( max_length=100000,allow_empty_file=False,use_url=False))

    # def create(self, validated_data):
    #     files = validated_data.pop('file')
    #     for f in files:
    #         File.objects.create(file=f)
    #     return []
    class Meta:
        model = Gallery
        fields = ('page', 'gallery_type', 'image')


class UploadView(APIView):
    parser_class = (FileUploadParser)

    def post(self, request, *args, **kwargs):
        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            print('error', file_serializer.errors)
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# Testing of Tags Creation
# Create and Update Social Links
