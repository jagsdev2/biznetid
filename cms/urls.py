from django.urls import path
from .views import *
from cms.view.page_details import *
from cms.view.gallery import GalleryCreateView, ImageDetailView
from cms.view.tags import TagDetailView, SericeDetailView

app_name = "cms"

urlpatterns = [
    path('', index, name="index"),
    path('home', CmsHome.as_view(), name="home"),
    path('page', ListPage.as_view(), name="page"),
    path('page/<int:pk>', PageDetailView.as_view(), name="updatepage"),
    path('address', AddressCreate.as_view(), name="createaddress"),
    path('address/<int:id>', AddressCreate.as_view(), name="upateaddress"),
    path('tags', TagDeatils.as_view(), name="tagscreate"),
    path('tags/<int:id>', TagDeatils.as_view(), name="tagsupdate"),
    path('gallery', GalleryCreate.as_view(), name="gallerycreate"),
    path('gallery/<int:pk>', ImageDetailView.as_view(), name="galleryupdate"),
    path('sociallinks', SocialLinkCreate.as_view(), name="sociallinks"),
    path('sociallinks/<int:id>', SocialLinkCreate.as_view(), name="sociallinks"),
    path('workinghours', OperationHoursCreate.as_view(), name="hourscreate"),
    path('workinghours/<int:id>', OperationHoursCreate.as_view(), name="hoursupdate"),
    path('reviews', ReviewsCreate.as_view(), name="reviewscreate"),
    path('reviews/<int:id>', ReviewsCreate.as_view(), name="reviewsupdate"),
    path('service', ServicesCreate.as_view(), name="servicecreate"),
    path('service/<int:id>', ServicesCreate.as_view(), name="servicupdate"),
    path('customlinks', CreateLinks.as_view(), name="createlinks"),
    path('customlinks/<int:id>', CreateLinks.as_view(), name="updatelinks"),
    path('contactus', ContactForm.as_view(), name="contactus"),
    path('contactus/<int:pk>', ContactDetailView.as_view(), name="updatemessages"),
    path('locationmap', LocationMap.as_view(), name="createmap"),
    path('locationmap/<int:id>', LocationMap.as_view(), name="updatemap"),
    path('paymentmethod', PaymentMethod.as_view(), name="paymentmethodcreate"),
    path('paymentmethod/<int:id>', PaymentMethod.as_view(), name="paymentmethodupdate"),
    path('viewtags/<int:id>', Viewtags.as_view(), name="viewtags"),
    path('serviceview/<int:id>', ServiceListView.as_view(), name="serviceview"),
    path('service-detail/<int:pk>', ServiceDetailView.as_view()),

    # new
    path("create-gallery",GalleryCreateView.as_view()),
    path("tag-detail/<int:pk>",TagDetailView.as_view()),
    path("servie-detail/<int:pk>",ServiceDetailView.as_view()),

]
