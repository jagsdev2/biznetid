# CMS / Serializer
from rest_framework import serializers
from django.contrib.auth import get_user_model
from rest_framework.relations import HyperlinkedRelatedField
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from .models import Page, Address, CustomLink, ServicesOfferd, Gallery, \
    OperationHours, Reviews, ContactUS, Location, PaymentMethod, PageSocialLink, TagItems

User = get_user_model()


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ['id','name', 'building_number', 'street', 'city', 'state', 'pincode', 'page']


# Tags for Page
class TagItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagItems
        fields = ['id','page', 'tag']


class GallerySerializer(serializers.ModelSerializer):
    # image = serializers.ListField(child=serializers.ImageField(allow_empty_file=False))

    class Meta:
        model = Gallery
        fields = ['id','image', 'page']

    def create(self, validated_data):
        images = validated_data.pop('image')
        for image in images:
            photo = Gallery.objects.create(image=image, **validated_data)
        return photo


class SocialLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSocialLink
        fields = ['id','page', 'name', 'fb_url', 'twitter_url', 'instagram_url', 'linkedin_url', 'youtube_url',
                  'whatsapp_num', 'is_active']


class OperationHoursSerializer(serializers.ModelSerializer):
    class Meta:
        model = OperationHours
        fields = ['id','monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'page']


class ReviewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reviews
        fields = ['id','name', 'email', 'content', 'rating', 'page']


class ServicesOfferdSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServicesOfferd
        fields = ['id','page', 'service']



class CustomLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomLink
        fields = ['id','name', 'url', 'page']


class PaymentMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentMethod
        fields = ['id','payment_mode', 'page']


class LocationMapSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ['id','place', 'lat', 'lon', 'page']


class PageSerializer(serializers.ModelSerializer):
    page_address = AddressSerializer(read_only=True)
    page_sociallink = SocialLinkSerializer(many=True, read_only=True)
    page_customlink = CustomLinkSerializer(read_only=True)
    page_services = ServicesOfferdSerializer(many=True,read_only=True)
    page_hours = OperationHoursSerializer(read_only=True)
    page_gallery = GallerySerializer(many=True, read_only=True)
    page_reviews = ReviewsSerializer(many=True, read_only=True)
    page_location = LocationMapSerializer(many=True, read_only=True)
    page_paymodes = PaymentMethodSerializer(many=True, read_only=True)
    page_tagitems = TagItemsSerializer(many=True, read_only=True)

    class Meta:
        model = Page

        # print("Page Address :", page_adr)
        fields = ['id','name', 'site', 'short_desc', 'banner', 'logo', 'phone_number', 'business_email',
                  'about', 'page_address', 'page_customlink', 'page_services', 'page_hours',
                  'page_gallery', 'page_reviews', 'page_sociallink', 'page_location', 'page_paymodes', 'page_tagitems']
        # depth = 1


class CreatePageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = ['name', 'short_desc', 'banner', 'logo', 'phone_number', 'business_email', 'about', 'site']


class ContactFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactUS
        fields = ['id', 'name', 'email', 'mobile', 'message', 'page', 'status']


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = (
            'page',
            'gallery_type',
            'image'
        )

