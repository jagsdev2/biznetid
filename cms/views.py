# CMS / Views
from django.http import HttpResponse, JsonResponse
from datetime import datetime, timedelta

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics
from .models import Page
from .permissions import IsWorking
from rbac.permissions import IsOwnerOnly, IsPageOwner, IsPageOwner2
from .serializer import PageSerializer, CreatePageSerializer
from listing.models import List
from packages.models import Package
from datetime import datetime, timezone
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from .permissions import IsPageCreator
from listing.serializer import PageDetailSerializer
# Define Global Variables
date_now = datetime.now(timezone.utc)


# Create your views here.
def index(request):
    return HttpResponse("Welcome to CMS APP !")


class CmsHome(APIView):
    # authentication_classes = [TokenAuthentication]
    permission_classes = (IsAuthenticated, IsWorking)

    def get(self, request):
        user = request.user
        # page_home = Page.objects.get(user=user)
        # serializer = PageSerializer(page_home)
        if user.user_scope == "END_USER" and user.package is not None:
            if date_now > user.package_expiry:
                print("Logged User :", user)
                status_msg = "Your package Has been Expired, Please choose another Package"
                return Response(status_msg, status=status.HTTP_423_LOCKED)
            else:
                status_msg = "Welcome Mr." + str(user.username)
                return Response(status_msg, status=status.HTTP_200_OK)

        elif user.user_scope == "END_USER" and user.package is None:
            status_message = "Please choose any package"
            return Response(status_message, status=status.HTTP_401_UNAUTHORIZED)

        else:
            status_message = "Incorrect User type, You are not allowed to Visit this page."
            return Response(status_message, status=status.HTTP_403_FORBIDDEN)

    def post(self, request, *args, **kwargs):
        user = request.user
        package_id = request.data['package_id']
        package = Package.objects.get(id=package_id)
        try:
            if date_now > user.package_expiry:
                is_expired = True
            else:
                is_expired = False
        except:
            is_expired = True

        if user.user_scope == "END_USER" and is_expired:
            if package.is_free:
                user.package = package
                vaild_days = int(package.validity)
                exp_date = datetime.today() + timedelta(vaild_days)
                user.package_expiry = exp_date
                user.save()
                status_message = "Package Updated Success fully"
                return Response(status_message, status=status.HTTP_200_OK)
            else:
                status_message = "Selected package is NOT free, Please make Payment"
                return Response(status_message, status=status.HTTP_402_PAYMENT_REQUIRED)
        else:
            status_message = "Selected User Role is Not Supported"
            return Response(status_message, status=status.HTTP_406_NOT_ACCEPTABLE)


# Set Paid package - Test function
def paidPackage(user, package):
    #package_name = Package.objects.get(id=package)
    package_id = package
    print("Package :", package, package)
    if user.user_scope == "END_USER":
        if package:
            user.package = package_id
            vaild_days = int(package.validity)
            exp_date = datetime.today() + timedelta(vaild_days)
            user.package_expiry = exp_date
            user.save()
            status_message = "Package Updated Success fully"
            return Response(status_message, status=status.HTTP_200_OK)
        else:
            status_message = "Selected package is NOT set, Please try again"
            return Response(status_message, status=status.HTTP_402_PAYMENT_REQUIRED)
    else:
        status_message = "Selected User Role is Not Supported"
        return Response(status_message, status=status.HTTP_406_NOT_ACCEPTABLE)


# Create View for Visiting card

class ListPage(APIView):
    # authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    parser_class = (FileUploadParser)

    def get(self, request):
        # list = self.get_object(slug)
        try:
            page = Page.objects.filter(user=request.user)
            page_serializer = PageSerializer(page, many=True)
            status_msg = page_serializer.data
            return Response(status_msg, status.HTTP_200_OK)

        except List.DoesNotExist:
            status_msg = "The requested URL was not found"
            return Response(status_msg, status=status.HTTP_404_NOT_FOUND)

    def post(self, request):
        serializer = PageSerializer(data=request.data)

        if serializer.is_valid():
            form_data = request.data
            user = request.user
            site_id = form_data['site']
            name = form_data['name']
            short_desc = form_data['short_desc']
            phone_number = form_data['phone_number']
            business_email = form_data['business_email']
            about = form_data['about']
            try:
                site = List.objects.get(id=site_id)
            except:
                status_msg = "No Listing URL ID, selected for create page."
                return Response(status_msg, status=status.HTTP_400_BAD_REQUEST)
            try:
                banner = form_data['banner']
                logo = form_data['logo']

            except:
                banner = None
                logo = None
            new_page = Page.objects.create(user=user, site=site, name=name, short_desc=short_desc, banner=banner,
                                           logo=logo,
                                           phone_number=phone_number, business_email=business_email, about=about)
            index1 = {"page_id": new_page.id}
            index1.update(serializer.data)
            index1.update(serializer.data)
            return Response(index1, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PageDetailView(generics.UpdateAPIView):
    permission_classes = [IsAuthenticated, IsPageCreator]
    serializer_class = PageDetailSerializer
    queryset = Page.objects.all()

class UpdatePage(APIView):
    # authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, IsPageOwner2]
    parser_class = (FileUploadParser)

    def get_object(self, id):
        try:
            ls = Page.objects.get(id=id)
            return ls

        except List.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        page = self.get_object(id)
        serializer = PageSerializer(page)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        page = self.get_object(id)
        serializer = PageSerializer(page, data=request.data)
        if serializer.is_valid():
            serializer.save()
            status_msg = "Successfully Updated"
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        page = self.get_object(id)
        page.delete()
        msg = "Page Successfully Deleted"
        return Response(msg, status=status.HTTP_204_NO_CONTENT)


class PageCreateView(APIView):
    # permission_classes = [IsAuthenticated]
    parser_classes = (MultiPartParser, FormParser)

    def get(self, request, *args, **kwargs):
        posts = Page.objects.all()
        serializer = CreatePageSerializer(posts, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        posts_serializer = CreatePageSerializer(data=request.data)
        if posts_serializer.is_valid():
            posts_serializer.save()
            return Response(posts_serializer.data, status=status.HTTP_201_CREATED)
        else:
            print('error', posts_serializer.errors)
            return Response(posts_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
