# Affiliate / Models.py
from django.contrib import admin
from .models import Affiliates,BankAccounts

# Register your models here.
admin.site.register(Affiliates)
admin.site.register(BankAccounts)