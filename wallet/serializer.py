#Wallet/Serialzer
from rest_framework import serializers
from .models import Transactions


class WalletTxnSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transactions
        fields = ['status', 'type', 'amount', 'created_at']
