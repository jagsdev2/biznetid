import smtplib
from email.mime.text import MIMEText as text
from django.test import TestCase


# Create your tests here.

def send_email(sender, receiver, message, subject):
    sender = sender
    receivers = receiver
    m = text(message)
    m['Test Django mail'] = subject
    m['no-reply@jagsinventions.com'] = sender
    m['dev2@jagsinventions.com'] = receiver

    # message = message

    try:
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, receivers, str(m))
        print("Successfully sent email")
    except smtplib.SMTPException:
        print("Error: unable to send email")


send_email("no-reply@jagsinventions.com","dev2@jagsinventions.com","Test message", "test Message")