# Wallet / Admin.py
from django.contrib import admin
from .models import Wallet, Transactions, WalletPayout

# Register your models here.
admin.site.register(Wallet)
admin.site.register(Transactions)
admin.site.register(WalletPayout)