# Wallet/views.py
from django.shortcuts import render
import string
import random
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
# Create your views here.
from rest_framework.views import APIView
from .models import Transactions, WalletPayout
from rest_framework.generics import RetrieveAPIView
from .serializer import WalletTxnSerializer


def index(request):
    return Response("Welcome to Wallet App")


# Display wallet Balance and Withdrawals Initiation
class WithdrawWallet(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = request.user
        try:
            wallet_balance = user.wallet.primary_wallet
            status_msg = "Your wallet balance is :" + str(wallet_balance)
            return Response(status_msg, status=status.HTTP_200_OK)

        except:
            status_msg = "You dont Have any wallet, Please Activate."
            return Response(status_msg, status=status.HTTP_204_NO_CONTENT)

    def post(self, request):
        user = request.user
        print("User:", user)
        try:
            wallet_balance = user.wallet.primary_wallet
            amount = float(request.data['amount'])
            wallet = user.wallet
            wallet_balance = user.wallet.primary_wallet
            if amount <= wallet_balance:
                aff_transaction = Transactions.objects.create(wallet=wallet, amount=amount, type="DEBIT")
                user.wallet.subtract_primary_wallet(amount)
                status_msg = "Your Withdrawal request is created"
                return Response(status_msg, status=status.HTTP_201_CREATED)
            else:
                status_msg = "Your request amount is less than Available wallet balance !"
                return Response(status_msg, status=status.HTTP_406_NOT_ACCEPTABLE)

        except Exception as e:
            print("Exemption", e)
            status_msg = "You dont Have any wallet, Please Activate."
            return Response(status_msg, status=status.HTTP_204_NO_CONTENT)


# Admin Approval of Payment Withdraw request and Generate Payout Sheet
class ApprovePayout(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        withdraw_request = Transactions.objects.get(id=id)
        withdraw_status = withdraw_request.status
        withdraw_amount = withdraw_request.amount
        print("Withdraw status:", withdraw_amount, withdraw_status)
        status_msg = "Your Withdraw request Status"
        return Response(status_msg, status=status.HTTP_200_OK)

    def post(self, request, id):
        withdraw_request = Transactions.objects.get(id=id)
        withdraw_status = withdraw_request.status
        withdraw_amount = withdraw_request.amount
        user = withdraw_request.wallet.user
        payout_bankacc = user.affiliates.bankaccount.bank_acc_num
        payout_bankifsc = user.affiliates.bankaccount.bank_ifsc_code
        payout_upid = user.affiliates.bankaccount.upi_id
        txn_id = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8)) + str(random.randint(1000, 9999))
        remarks = "BiznetID Affiliate payout"
        payout = WalletPayout.objects.create(user=user, name=user.first_name, payout_amount=withdraw_amount,
                                             txn_id=txn_id,
                                             remarks=remarks, payout_bankacc=payout_bankacc,
                                             payout_bankifsc=payout_bankifsc, payout_upid=payout_upid)
        print("User:", user)
        status_msg = "Your Withdraw request Approved"
        return Response(status_msg, status=status.HTTP_201_CREATED)


class BankaccDetails(RetrieveAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = request.user
        print("user :", user)
        try:
            wallet = user.wallet.id
            accdata = Transactions.objects.filter(wallet=wallet)
            serializer = WalletTxnSerializer(accdata, many=True)
            status_msg = serializer.data
            return Response(status_msg, status=status.HTTP_200_OK)
        except:
            status_msg = "Transactions Not Available"
            return Response(status_msg, status=status.HTTP_404_NOT_FOUND)
