from django.urls import path
from .views import *

app_name = "payments"

urlpatterns = [
    path('', index, name="home"),
    path('checkout', Checkout.as_view(), name="checkout"),
    path('pay', StartPayment.as_view(), name="payment"),
    path('success', handle_payment_success, name="payment_success"),
    path('transaction', Transactions.as_view(), name=''),
    # path('confirm_order', create_order, name='create_order'),
    # path('payment_status', payment_status, name='payment_status'),
]
