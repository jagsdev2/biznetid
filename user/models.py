
# Create your models here.
# user/models.py

from django.db import models
from django.core.mail import send_mail
from django.db import models
from django.contrib.auth.models import AbstractUser
import uuid
import datetime
from django.utils import timezone
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse
from django.conf import settings
from decouple import config

class User(AbstractUser):
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username"]

    USER_SCOPES = [("END_USER", "End User"), ("VENDOR", "Vendor"), ("ADMIN", "Admin"), ("SUPERADMIN", "Super Admin")]
    USER_STATUS = [("ACTIVE", "Active"), ("BLOCKED", "Blocked"), ("NOT_VERIFIED", "Not Verified"),
                   ("VERIFIED", "Verified")]

    email = models.EmailField(blank=True, max_length=254, verbose_name="email address", unique=True)
    phone_number = models.CharField(max_length=15, null=True)
    country = models.CharField(max_length=100)
    country_code = models.CharField(max_length=10, null=True)
    is_active = models.BooleanField(default=False)
    user_scope = models.CharField(max_length=50, choices=USER_SCOPES, default="END_USER")
    user_status = models.CharField(max_length=30, choices=USER_STATUS, default="NOT_VERIFIED")
    uuid = models.UUIDField(default=uuid.uuid4(), editable=False)
    is_affiliate = models.BooleanField(default=False)
    package = models.ForeignKey('packages.Package', on_delete=models.DO_NOTHING, null=True)
    package_expiry = models.DateTimeField(null=True)
    referred_by = models.CharField(max_length=50, null=True, blank=True)
    created_by = models.ForeignKey('self', related_name="created_users", on_delete=models.DO_NOTHING, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.username)


# Created a Model for Generating and saving the User Email ID Verification token
class Verification(models.Model):
    user = models.OneToOneField(User, related_name="signup_url", on_delete=models.DO_NOTHING)
    uuid = models.UUIDField(null=True, blank=True, unique=True, editable=False)
    is_used = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def is_expired(self):
        today = timezone.now()
        difference = self.created_at - today
        is_expired = difference >= datetime.timedelta(days=30)
        return is_expired

    def save(self, *args, **kwargs):
        self.uuid = uuid.uuid4()
        super(Verification, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.user)


# Signals for Create Email Id verification Token and Send User Verification Email
@receiver([post_save], sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Verification.objects.create(user=instance)
        user = Verification.objects.get(user=instance)
        user_token = user.uuid
        send_verification_mail(instance, user_token)


def send_verification_mail(obj, user_token):
    subject = "JAGS - Verify your Email ID"
    from_email = config("NOREPLYMAIL")
    to_email = str(obj.email)
    base_domain = settings.DOMAIN_ROOT
    try:
        verification_ur = str(base_domain) + "/user/verify-email/" + str(user_token)
        message = "JAGS Business card Account - Status" + '\n' + str(verification_ur)
        send_mail(subject, message, from_email, [to_email])
    except BadHeaderError:
        return HttpResponse('Could not send mail, contact support')