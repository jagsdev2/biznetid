# Users / Views.py
from django.shortcuts import render
from django.contrib.auth import get_user_model
from django.http import HttpResponse, JsonResponse
from django.http import Http404
from django.db import transaction
from django.shortcuts import get_object_or_404
from django.core.exceptions import ValidationError
from django.core.mail import send_mail, BadHeaderError
from django.conf import settings
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from .models import Verification
from .serializers import UserSerializer, DetailedUserSerializer

import uuid

User = get_user_model()


# Extra codes for Testing the User Registration
def home(request):
    context = {"username": User.username}
    return HttpResponse("User Home")


class VerifyEmail(APIView):

    def get(self, request, uid):
        try:
            code = get_object_or_404(Verification, uuid=uid)
            # user = code.user
            user = User.objects.get(username=code.user.username)
            user.is_active = True
            user.user_status = "VERIFIED"
            with transaction.atomic():
                user.save()
                code.delete()
            return Response(status=status.HTTP_200_OK)

        except ValidationError:
            raise Http404()


# Resend verification email
class ResendVerifyMail(APIView):
    def post(self, request):
        try:
            email = request.data.get("email")
            user = User.objects.get(email=email)
            if user.user_status == "VERIFIED":
                status_msg = "User Email Address is Already verified"
                return Response(status_msg, status=status.HTTP_200_OK)
            else:
                try:
                    uid = user.signup_url.uuid

                except Exception as e:
                    uid = uuid.uuid4()
                    Verification.objects.create(user=user, uuid=uid)
                if uid is not None:
                    status_msg = "An Email has been send to Your registered Email ID"
                    print("UUID :", )
                    subject = "JAGS - Verify your Email ID"
                    from_email = "no-reply@cushbin.com"
                    to_email = str(user.email)
                    base_domain = settings.DOMAIN_ROOT
                    user_token = user.signup_url.uuid
                    try:
                        verification_ur = str(base_domain) + "/api/v1/user/verify-email/" + str(user_token)
                        message = "JAGS Business card Account - Status" + '\n' + str(verification_ur)
                        send_mail(subject, message, from_email, [to_email])
                    except BadHeaderError:
                        return HttpResponse('Could not send mail, contact support')

                else:
                    status_msg = "No UUID exist for this user"
                return Response(status_msg, status=status.HTTP_200_OK)

        except ValidationError:
            raise Http404()


# Django REST API with serializer - For List Users and Register user
class UserAPIView(APIView):

    def get(self, request):
        articles = User.objects.all()
        serializer = UserSerializer(articles, many=True)
        return Response(serializer.data)

    def post(self, request):
        # serializer = UserSerializer(data=request.data)
        serializer = RegisterSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# API View for  Retrieve User data, Update and delete User data
class UserDetails(APIView):
    # authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_object(self, id):
        try:
            return User.objects.get(id=id)

        except User.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request):
        id = request.user.id
        article = self.get_object(id)
        # serializer = UserSerializer(article)
        serializer = DetailedUserSerializer(article)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    def put(self, request):
        id = request.user.id
        article = self.get_object(id)
        serializer = UserSerializer(article, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        id = request.user.id
        article = self.get_object(id)
        article.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TestView(APIView):
    # authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    # permission_classes = (AllowAny,)
    def get(self, request):
        status_msg = "Authentication Succesfull ..!"
        return Response(status_msg, status=status.HTTP_202_ACCEPTED)
