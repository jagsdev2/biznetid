#Users/Urls
from django.urls import path
from .views import *


app_name = "user"

urlpatterns = [
    path("verify-email/<str:uid>", VerifyEmail.as_view()),
    path("resend-email", ResendVerifyMail.as_view()),
    path("home", home, name="home"),
    path("list", UserAPIView.as_view(), name="user"),
    path('detail', UserDetails.as_view(), name="userdetails"),
    path("test", TestView.as_view(), name="test"),

]