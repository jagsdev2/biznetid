# Listing /views.py
import generics as generics
from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse, JsonResponse
from cms.serializer import PageSerializer
from .models import List, Services
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, generics
from .serializer import ListSerializer, ServiceSerializer, \
    PersonnelCardSerializer, VisitingCardSerializer, UpdateCardSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework import permissions
from .permissions import IsOwnerOnly
from rbac.permissions import IsAdminOnly, IsGetOrIsAuthenticated, IsPageOwner, IsCardOwner
from cms.models import VisitingCard, Page
from rest_framework import generics

# Create your views here.
def index(request):
    return HttpResponse("Welcome to App Home")


# List Loged user List on His dash Board
class MyListAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = request.user
        listing = List.objects.filter(user=user)
        serializer = ListSerializer(listing, many=True)
        return Response(serializer.data)


# List Customer Business Summary and Update
class ListAPIView(APIView):
    permission_classes = [IsGetOrIsAuthenticated]

    def get(self, request):
        listing = List.objects.all()
        serializer = ListSerializer(listing, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = ListSerializer(data=request.data)

        if serializer.is_valid():
            # serializer.save()
            user_id = request.user.id
            template = request.data['template']
            template_id = int(template)  # Templates.objects.get(id=template)
            url_name = request.data['url_name']
            list_type = request.data['type']
            if len(url_name) < 4:
                error_msg = "URL Must be Four or More Characters"
                return Response(error_msg, status=status.HTTP_406_NOT_ACCEPTABLE)

            list = List.objects.create(user_id=user_id, type=list_type, url_name=url_name, template_id=template_id,
                                       status=True, )
            index1 = {"site_id": list.id}  # Fetch Database ID for the new List
            index1.update(serializer.data)  # Append the Serializer data to Index id
            index1.update(serializer.data)

            return Response(index1, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# List Customer Service Details and Create
class ServiceAPIView(APIView):
    # authentication_classes = [TokenAuthentication]
    permission_classes = [IsGetOrIsAuthenticated]

    def get(self, request):
        service = Services.objects.all()
        serializer = ServiceSerializer(service, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = ServiceSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# List User Business Slug url via Slug URL
class PageDetails(APIView):

    def get(self,request, slug):
        obj = get_object_or_404(List, url_name=slug)
        serializer = ListSerializer(obj)
        return Response(serializer.data)


    # def get(self, request, slug):
    #     # list = self.get_object(slug)
    #     try:
    #         listing = List.objects.get(url_name=slug)
    #         if listing.template is not None:
    #             list_serializer = ListSerializer(listing)
    #             page_home = listing.page_listing
    #             page_serializer = PageSerializer(page_home)
    #             status_msg = page_serializer.data   #list_serializer.data
    #             return Response(status_msg, status.HTTP_200_OK)
    #         else:
    #             status_msg = "Requested Page Does not set any template"
    #             return Response(status_msg, status.HTTP_423_LOCKED)

    #     except List.DoesNotExist:
    #         status_msg = "The requested URL was not found"
    #         return Response(status_msg, status=status.HTTP_404_NOT_FOUND)


# Update and Delete the List
class ListDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsOwnerOnly)
    serializer_class = ListSerializer
    queryset = List.objects.all()

    # def get_object(self, id):
    #     try:
    #         ls = List.objects.get(id=id)
    #         return ls

    #     except List.DoesNotExist:
    #         return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    # def get(self, request, id):
    #     listing = self.get_object(id)
    #     # serializer = ListSerializer(listing)
    #     page_home = listing.page_listing
    #     try:
    #         page_home = listing.page_listing
    #     except RelatedObjectDoesNotExist:
    #         page_home = {}
    #     page_serializer = PageSerializer(page_home)
    #     status_msg = page_serializer.data
    #     return Response(status_msg, status=status.HTTP_200_OK)

    # def put(self, request, id):
    #     listing = self.get_object(id)
    #     serializer = ListSerializer(listing, data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
    #     return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def delete(self, request, id):
    #     listing = self.get_object(id)
    #     listing.delete()
    #     msg = "Successfully Deleted"
    #     return Response(msg, status=status.HTTP_204_NO_CONTENT)


# Display Visiting card inside a Page Listing.
class PersonnelCard(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, slug, card):
        current_listing = List.objects.get(url_name=slug)
        current_page = current_listing.page_listing
        page_serializer = PageSerializer(current_page)
        page_details = page_serializer.data
        current_visitng_card = VisitingCard.objects.get(page=current_page, card_url=card)
        card_serializer = PersonnelCardSerializer(current_visitng_card)
        response = card_serializer.data
        return Response(response, status.HTTP_200_OK)
        #        visit_card = VisitingCard.objects.all()


# Create Visiting card inside a Page Listing.
class CreatePersonnelcard(APIView):
    permission_classes = [IsAuthenticated, IsPageOwner]
    serializer_class = VisitingCardSerializer

    def get(self, request):
        status_msg = "Method is working"
        return Response(status_msg, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = VisitingCardSerializer(data=request.data)
        if serializer.is_valid():
            card_data = request.data
            page = card_data['page']
            card_url = card_data['card_url']
            name = card_data['name']
            mobile = card_data['mobile']
            designation = card_data['designation']
            whatsapp = card_data['whatsapp']
            facebook = card_data['facebook']
            instagram = card_data['instagram']
            youtube = card_data['youtube']
            linkedin = card_data['linkedin']
            official_email = card_data['official_email']
            mypage = Page.objects.get(id=page)
            try:
                requested_card = VisitingCard.objects.get(page=page, card_url=card_url)
            except:
                requested_card = None
            if requested_card is None:
                vcard = VisitingCard.objects.create(page=mypage, card_url=card_url, name=name, mobile=mobile,
                                                    designation=designation,
                                                    whatsapp=whatsapp, facebook=facebook, instagram=instagram,
                                                    youtube=youtube, linkedin=linkedin, official_email=official_email)
                status_msg = "Successfully created Visiting card"
                return Response(status_msg, status=status.HTTP_201_CREATED)
            else:
                status_msg = "Requested Visiting card is already exist"
                return Response(status_msg, status=status.HTTP_406_NOT_ACCEPTABLE)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Update Visiting card
class UpdatePersonnelcard(generics.UpdateAPIView):
    permission_classes = [IsAuthenticated, IsCardOwner]
    serializer_class = VisitingCardSerializer

    def get_object(self, id):
        try:
            ls = VisitingCard.objects.get(id=id)
            return ls

        except VisitingCard.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def put(self, request, id):
        article = self.get_object(id)
        serializer = UpdateCardSerializer(article, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
