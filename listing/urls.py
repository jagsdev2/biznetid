# Listing / Urls.py
from django.urls import path
from .views import *
from rest_framework.routers import DefaultRouter
router = DefaultRouter()

app_name = "listing"

urlpatterns = [
    path('', index, name="home"),
    path('list-detail/<int:pk>', ListDetailView.as_view(), name="list-details"),
    path('list', ListAPIView.as_view(), name="read"),
    path('read', MyListAPIView.as_view(), name="read"),
    path('create', ListAPIView.as_view(), name="create"),
    path('list/<str:slug>', PageDetails.as_view(), name="list"),
    path('list/<str:slug>/<str:card>', PersonnelCard.as_view(), name="visitngcard"),
    path('createcard', CreatePersonnelcard.as_view(), name="createcard"),
    path('updatecard/<int:id>', UpdatePersonnelcard.as_view(), name="updatecard"),
    # path('service/', ServiceAPIView.as_view(), name="service"),
    # path('templates/', TemplateAPIView.as_view(), name="templates"),
    # path('template/<int:id>', TemplateDetails.as_view(), name="template-details"),

]

