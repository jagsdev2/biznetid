# Listing/Models.py
from django.db import models
from django.contrib.auth import get_user_model
import uuid

User = get_user_model()


# Listing Model
class List(models.Model):
    LISTING_TYPES = [("BUSINESS", "Business"), ("PROFESSIONAL", "Professional")]
    user = models.ForeignKey('user.User', related_name='user_listing', on_delete=models.DO_NOTHING,
                             blank=True)
    type = models.CharField(max_length=50, choices=LISTING_TYPES, default="BUSINESS")
    url_name = models.CharField(max_length=250, unique=True)
    uuid = models.UUIDField(default=uuid.uuid4(), editable=False)
    template = models.ForeignKey('themes.Templates', related_name='template_list', on_delete=models.PROTECT, null=True)
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.url_name)


# ----------------------------------------------------------------------------------------
class Services(models.Model):
    user = models.ForeignKey('user.User', on_delete=models.DO_NOTHING, null=True, blank=True)
    list = models.ForeignKey('List', on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(max_length=500, null=True, blank=True)
    amount = models.FloatField(default=0.00)
    status = models.BooleanField(default=False)
    created_by = models.ForeignKey('user.User', related_name='service_user', blank=True, on_delete=models.PROTECT,
                                   null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)

    class meta:
        db_table = "Services"
