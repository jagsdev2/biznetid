# Listing / Serializer.py
from rest_framework import serializers
from .models import List, Services
from themes.models import Templates
from cms.models import (VisitingCard, Page, Address,PageSocialLink,
 TagItems,ServicesOfferd, Gallery)


class PageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = ['name', 'short_desc', 'logo','banner','about']


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = '__all__'

class PageSocialLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSocialLink
        fields = ['fb_url','twitter_url','instagram_url','linkedin_url','youtube_url','whatsapp_num','is_active','created_at']

class PageTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagItems
        fields = ['tag','id']

class PageServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServicesOfferd
        fields = ['service','id']

class PageGallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = ['image','id']

class PageDetailSerializer(serializers.ModelSerializer):
    page_address = AddressSerializer(read_only=True)
    page_sociallink = PageSocialLinkSerializer(read_only=True)
    page_tagitems = PageTagSerializer(read_only=True, many=True)
    page_services = PageServiceSerializer(read_only=True, many=True)
    page_gallery= PageGallerySerializer(read_only=True, many=True)
    class Meta:
        model = Page
        fields = '__all__'
# 'page_address','page_sociallink','page_customlink','page_tagitems','page_services','page_hours','page_gallery'



class ListSerializer(serializers.ModelSerializer):
    page_listing = PageDetailSerializer(read_only=True)
    class Meta:
        model = List
        fields = ('id', 'url_name', 'type', 'template','page_listing')
        read_only_fields = ['page_listing']





    # def validate(self, data):
    #     url_name = data['url_name']
    #     type = data['type']
    #     print("**** ***** ** working :", url_name)

    #     if len(url_name) < 4:
    #         raise serializers.ValidationError(
    #             {"url_name": "URL name should be at least four charters."}
    #         )

    #     return data


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Services
        fields = '__all__'


class PersonnelCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisitingCard
        fields = ('id', 'card_url', 'name', 'mobile', 'designation', 'whatsapp', 'facebook', 'instagram',
                  'youtube', 'linkedin', 'official_email')


class VisitingCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisitingCard
        fields = ['id', 'page', 'card_url', 'name', 'mobile', 'designation', 'whatsapp', 'facebook', 'instagram',
                  'youtube', 'linkedin', 'official_email']


class UpdateCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisitingCard
        fields = ['name', 'mobile', 'designation', 'whatsapp', 'facebook', 'instagram',
                  'youtube', 'linkedin', 'official_email']

    # def update(self, instance, validated_data):
    #     instance.id = validated_data.get('id', instance.id)
    #     instance.page = validated_data.get('page', instance.name)
    #     instance.card_url = validated_data.get('card_url', instance.card_url)
    #     instance.name = validated_data.get('official_email', instance.name)
    #     instance.mobile = validated_data.get('mobile', instance.mobile)
    #     instance.designation = validated_data.get('designation', instance.designation)
    #     instance.whatsapp = validated_data.get('whatsapp', instance.whatsapp)
    #     instance.facebook = validated_data.get('facebook', instance.facebook)
    #     instance.instagram = validated_data.get('instagram', instance.instagram)
    #     instance.youtube = validated_data.get('youtube', instance.youtube)
    #     instance.linkedin = validated_data.get('linkedin', instance.linkedin)
    #     instance.official_email = validated_data.get('official_email', instance.official_email)
    #     instance.save()
    #     return instance
