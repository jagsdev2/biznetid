# Listing/Permissions.py
from rest_framework import permissions
from .models import List, Services
from django.shortcuts import get_object_or_404


class IsOwnerOnly(permissions.BasePermission):

    def has_permission(self, request, view):

        obj = get_object_or_404(List,id=view.kwargs['pk'])
        return obj.page_listing.user == request.user
 