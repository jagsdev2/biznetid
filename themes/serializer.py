# Themes / Serializer.py
from rest_framework import serializers
from themes.models import Templates


class TemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Templates
        fields = '__all__'
