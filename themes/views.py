# Themes / Views.py
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rbac.permissions import IsOwnerOnly
from themes.models import Templates
from .serializer import TemplateSerializer
from rbac.permissions import IsAdminOnly, IsGetOrIsAdmin


# Create your Views Here
def index(request):
    return HttpResponse("Themes App")


# List Template Details and Create
class TemplateAPIView(APIView):
    permission_classes = [IsGetOrIsAdmin]

    def get(self, request):
        template = Templates.objects.all()
        serializer = TemplateSerializer(template, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = TemplateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# List Customer Business Details and Update
class TemplateDetails(APIView):
    serializer_class = TemplateSerializer

    permission_classes = [IsAdminOnly]
    queryset = Templates.objects.all()

    def get_object(self, id):
        try:
            return Templates.objects.get(id=id)

        except Templates.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        template = self.get_object(id)
        serializer = TemplateSerializer(template)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        template = self.get_object(id)
        serializer = TemplateSerializer(template, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        template = self.get_object(id)
        template.delete()
        msg = "Successfully Deleted"
        return Response(msg, status=status.HTTP_204_NO_CONTENT)


class SetPaidTheemes(APIView):
    # permission class - is owner onlw

    def get(self, request):
        pass
        # Add Logic for Set a paid template within his package

    def post(self, request):
        pass
        # Add Logic for Set a paid template within his package
