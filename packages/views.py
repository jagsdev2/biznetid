# packages/views.py

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListCreateAPIView
from rbac.permissions import IsAdminOnly, IsGetOrIsAdmin
from .models import Package, Features
from rest_framework.authentication import TokenAuthentication
from .serializer import PackageSerializer, FeatureSerializer


# Create your views here.
def index(request):
    return HttpResponse("Welcome to App Home")


# List All PackagesDetails and Create
class PackageAPIView(APIView):
    # authentication_classes = [TokenAuthentication]
    permission_classes = [IsGetOrIsAdmin]

    def get(self, request):
        package = Package.objects.filter(status=True)
        serializer = PackageSerializer(package, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = PackageSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# List Package Details and Update
class PackageDetails(ListCreateAPIView):
    # authentication_classes = [TokenAuthentication]
    serializer_class = PackageSerializer
    permission_classes = [IsAdminOnly]

    def get_object(self, id):
        try:
            return Package.objects.get(id=id)

        except Package.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        print("template ID :", id)
        template = self.get_object(id)
        serializer = PackageSerializer(template)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        template = self.get_object(id)
        serializer = PackageSerializer(template, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        template = self.get_object(id)
        template.delete()
        msg = "Successfully Deleted"
        return Response(msg, status=status.HTTP_204_NO_CONTENT)


# List Feature Details and Create
class FeaturesAPIView(APIView):
    #authentication_classes = [TokenAuthentication]
    permission_classes = [IsGetOrIsAdmin]

    def get(self, request):
        template = Features.objects.all()
        serializer = FeatureSerializer(template, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = FeatureSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# List Package- Features Details and Update
class FeatureDetails(ListCreateAPIView):
    serializer_class = FeatureSerializer
    #authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdminOnly]

    # permission_classes = (IsAuthenticated, IsOwnerOnly)

    def get_object(self, id):
        try:
            return Features.objects.get(id=id)

        except Features.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        print("template ID :", id)
        template = self.get_object(id)
        serializer = FeatureSerializer(template)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        template = self.get_object(id)
        serializer = FeatureSerializer(template, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        template = self.get_object(id)
        template.delete()
        msg = "Successfully Deleted"
        return Response(msg, status=status.HTTP_204_NO_CONTENT)
