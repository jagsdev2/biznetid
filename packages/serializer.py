# Package/Serializer.py
from rest_framework import serializers
from .models import Package, Features


# Package Serializer
class PackageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Package
        fields = ['id', 'package_name', 'validity', 'features', 'price', 'status', 'is_free']
        # fields = '__all__'
        depth = 1


class FeatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Features
        fields = '__all__'
