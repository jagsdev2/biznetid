# Adminapp/Adminsettings.py
from django.contrib import messages
from django.utils import six
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render, redirect
from django.views import generic
from cms.models import PaymentTypes
from cms.forms import PaymentTypesForm


def group_required(group, login_url=None, raise_exception=False, ):
    """
    Decorator for views that checks whether a user has a group permission,
    redirecting to the log-in page if necessary.
    If the raise_exception parameter is given the PermissionDenied exception
    is raised.
    """

    def check_perms(user):
        if isinstance(group, six.string_types):
            groups = (group,)
        else:
            groups = group
        # First check if the user has the permission (even anon users)

        if user.groups.filter(name__in=groups).exists():
            return True

        # In case the 403 handler should be called raise the exception
        if raise_exception:
            raise PermissionDenied
        # As the last resort, show the login form
        return False

    return user_passes_test(check_perms, login_url=login_url)


# need to upopdate


class CreatePaymentTMethods(generic.View):
    def get(self, request):
        payment_types = PaymentTypes.objects.all()
        context = {'payment_types': payment_types}
        return render(request, 'adminapp/create_payment_methods.html', context)

    def post(self, request):
        form = PaymentTypesForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Payment method created Successfully')
            return redirect('/adminapp/view-payment-methods/')
        else:
            messages.add_message(request, messages.ERROR, 'Something Went Wrong.!')
            return redirect('/adminapp/view-payment-methods/')


class ViewPaymentTMethods(generic.View):
    def get(self, request):
        payment_types = PaymentTypes.objects.all()
        context = {'payment_types': payment_types}
        return render(request, 'adminapp/payment_types.html', context)
