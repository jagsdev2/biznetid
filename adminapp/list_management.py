# Adminapp/ List_management.py
from django.utils.decorators import method_decorator
from django.views import generic
from django.contrib import messages, auth
from django.shortcuts import render, redirect
from adminapp.views import session_required
from listing.models import List
from cms.models import VisitingCard
from django.contrib.auth import get_user_model

User = get_user_model()


# List All Pages
@method_decorator(session_required, name='dispatch')
class AllList(generic.View):

    def get(self, request):
        lists = List.objects.all()
        context = {'lists': lists}
        return render(request, 'adminapp/list_pages.html', context)


# Approve a Listing page
@session_required
def approveList(request, id):
    if request.method == 'POST':
        list = List.objects.get(id=id)
        list.status = True
        list.save()
        messages.add_message(request, messages.SUCCESS, 'List Approved Successfully')
        return redirect('/adminapp/all-list/')
    else:
        messages.add_message(request, messages.ERROR, 'Method NOT Alowed')
        return redirect('/adminapp/all-list/')


# Disable a Listing page
@session_required
def disableList(request, id):
    if request.method == 'POST':
        list = List.objects.get(id=id)
        list.status = False
        list.save()
        messages.add_message(request, messages.ERROR, 'List Disabled Successfully')
        return redirect('/adminapp/all-list/')
    else:
        messages.add_message(request, messages.ERROR, 'Method NOT Alowed')
        return redirect('/adminapp/all-list/')


# List All visiting cards inside a page

# List All Pages
@method_decorator(session_required, name='dispatch')
class AllCard(generic.View):

    def get(self, request):
        cards = VisitingCard.objects.all()
        context = {'cards': cards}
        for card in cards:
            print("Cards:", card)
        return render(request, 'adminapp/list_cards.html', context)


# Approve a Listing page
@session_required
def approveCard(request, id):
    if request.method == 'POST':
        cards = VisitingCard.objects.get(id=id)
        cards.status = True
        cards.save()
        messages.add_message(request, messages.SUCCESS, 'Card Approved Successfully')
        return redirect('/adminapp/all-cards/')
    else:
        messages.add_message(request, messages.ERROR, 'Method NOT Alowed')
        return redirect('/adminapp/all-cards/')


# Disable a Listing page
@session_required
def disableCard(request, id):
    if request.method == 'POST':
        cards = VisitingCard.objects.get(id=id)
        cards.status = False
        cards.save()
        messages.add_message(request, messages.ERROR, 'Card Disabled Successfully')
        return redirect('/adminapp/all-cards/')
    else:
        messages.add_message(request, messages.ERROR, 'Method NOT Alowed')
        return redirect('/adminapp/all-cards/')
