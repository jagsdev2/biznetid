# Adminapp/ Template_management.py
from django.utils.decorators import method_decorator
from django.views import generic
from django.contrib import messages, auth
from django.shortcuts import render, redirect

from adminapp.views import session_required
from themes.models import Templates
from packages.models import Package
from django.contrib.auth import get_user_model

User = get_user_model()


# List All Pages
@method_decorator(session_required, name='dispatch')
class AllThemes(generic.View):

    def get(self, request):
        themes = Templates.objects.all()
        context = {'themes': themes}
        return render(request, 'adminapp/themes.html', context)


@method_decorator(session_required, name='dispatch')
class CreateThemes(generic.View):
    def get(self, request):
        packages = Package.objects.all()
        context = {'packages': packages}
        return render(request, 'adminapp/create_themes.html', context)

    def post(self, request):
        name = request.POST.get('name')
        tid = request.POST.get('tid')
        is_free = request.POST.get('isfree')
        status = request.POST.get('status')
        bg_img = request.POST.get('bg_img')
        thump_img = request.POST.get('thump_img')
        packages = request.POST.get('packages')
        if is_free is not None:
            is_free = True
        else:
            is_free = False
        if status is not None:
            status = True
        else:
            status = False

        new_theme = Templates.objects.create(tid=tid, name=name, status=status, is_free=is_free,
                                             bg_image=bg_img, thumbnail=thump_img)
        new_theme.package.add(*packages)
        messages.add_message(request, messages.SUCCESS, 'Template created Successfully')
        return redirect('/adminapp/create-theme/')


@method_decorator(session_required, name='dispatch')
class UpdateteThemes(generic.View):
    def get(self, request, id):
        theme = Templates.objects.get(id=id)
        packages = Package.objects.all()
        context = {'theme': theme, 'packages': packages}
        return render(request, 'adminapp/update_theme.html', context)

    def post(self, request, id):
        name = request.POST.get('name')
        tid = request.POST.get('tid')
        is_free = request.POST.get('isfree')
        status = request.POST.get('status')
        bg_img = request.POST.get('bg_img')
        thump_img = request.POST.get('thump_img')
        packages = request.POST.get('packages')
        if is_free is not None:
            is_free = True
        else:
            is_free = False
        if status is not None:
            status = True
        else:
            status = False
        current_theme = Templates.objects.get(id=id)
        current_theme.tid = tid
        current_theme.is_free = is_free
        current_theme.status = status
        current_theme.bg_image = bg_img
        current_theme.thumbnail = thump_img
        current_theme.name = name
        current_theme.save()
        current_theme.package.add(*packages)  # Set features to the Newly created packages

        messages.add_message(request, messages.SUCCESS, 'Theme Updated Successfully')
        return redirect('/adminapp/all-themes/')

    def delete(self, request, id):
        package = Templates.objects.get(id=id)
        package.delete()
        messages.add_message(request, messages.WARNING, 'Theme Deleted Successfully')
        return redirect('/adminapp/all-themes/')
